import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from './auth.service';

export const environment = {
  apiKey: "AIzaSyBkRYwwxUkzlUMGMnpdOMi_2hPWZ-GFONw",
  authDomain: "authentication-5d702.firebaseapp.com",
  databaseURL: "https://authentication-5d702.firebaseio.com",
  projectId: "authentication-5d702",
  storageBucket: "",
  messagingSenderId: "1467927972"
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment),
    AngularFireAuthModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
