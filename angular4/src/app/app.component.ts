import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title2 = 'Curso de Angular 4 - VAMOS POR IONIC';
  title = 'Juegos en Angular4';
}
