import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	selector: 'contacto',
	templateUrl: './contacto.component.html'
})
export class ContactoComponent{
	public titulo = "Página de Contacto de la web";
	public parametro;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router
	){}

	ngOnInit(){
		//la funcion de flecha permite tener acceso a todas las variables de la clase
		this._route.params.forEach((params: Params) => {
			this.parametro = params['page'];
			console.log(this.parametro)
		})
	}

	redirigir(){
		this._router.navigate(['/contacto', 'daniloViacava.com'])
	}

	redirigirDos(){
		this._router.navigate(['/pagina-principal'])
	}
}