import { Component } from '@angular/core'

@Component({
	selector: 'juego',
	templateUrl: './fruta.component.html'
})
export class FrutaComponent{
	public nombre_componente = 'Componente Juego'
	public listado_frutas = 'Mario Bros, Candy crush, Template run'

	public nombre:string = 'David'
	public edad:number = 16;
	public mayorEdad:boolean = true;
	public trabajos:Array<any> = ['Carpintero, Cerrajero, Pintor '];
	comodin:any = 23 

	constructor(){

		this.nombre = "Roberto"

	}

	//Primer metodo que se lanza al crear la pagina
	ngOnInit(){
		this.cambiarNombre();

		// Variable y alcance
		// var = 
		// let = 

		var uno = 8;
		var dos = 15;

		if(uno === 8){
			let uno = 3;
			var dos = 88; 
			console.log("DENTRO DEL IF " + uno);
		} 
		console.log("FUERA DEL IF " + uno);
	}

	cambiarNombre(){
		this.nombre = 'Juan Lopez'
	}
 }