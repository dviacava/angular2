import { Component } from '@angular/core'
import { Empleado } from './empleado'

@Component({
	selector: 'empleado',
	templateUrl: './empleado.html'
})

export class EmpleadoComponent {
	public titulo = "Componente JUEGOS HTML5"
	public empleado:Empleado
	public trabajadores:Array<Empleado>
	public trabajador_externo:boolean;
	public color:string;
	public color_seleccionado:string;

	constructor(){
		this.empleado = new Empleado('Danilo Viacava', 23, 'programadador', true);
		this.trabajadores = [
			new Empleado('Danilo Viacava', 23, 'programadador', true),
			new Empleado('Adrian Viacava', 18, 'comunicador', false),
			new Empleado('Ada Viacava', 55, 'cocinera', false)
		];

		this.trabajador_externo = true;
		this.color = 'red';
		// this.color = 'blue';
		this.color_seleccionado = '#ccc'; 
	}

	ngOnInit(){
		console.log(this.empleado)
		console.log(this.trabajadores)
	}

	cambiarExterno(valor){
		this.trabajador_externo = valor;
	}

	logColorSeleccionado(){
		console.log(this.color_seleccionado);
	}

}