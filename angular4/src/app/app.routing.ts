import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { EmpleadoComponent } from './empleado/empleado.component';
import { FrutaComponent } from './fruta/fruta.component';
import { HomeComponent } from './home/home.component';
import { ContactoComponent } from './contacto/contacto.component';
import { CochesComponent } from './coches/coches.component';
import { PlantillasComponent } from './plantillas/plantillas.component';

const appRoutes: Routes = [
	//indice
	{path: '', component: HomeComponent},
	{path: 'empleado', component: EmpleadoComponent},
	{path: 'fruta', component: FrutaComponent},
	{path: 'pagina-principal', component: HomeComponent},
	{path: 'contacto', component: ContactoComponent},
	{path: 'contacto/:page', component: ContactoComponent},
	{path: 'coches', component: CochesComponent},
	{path: 'plantillas', component: PlantillasComponent},
	//ruta PlantillasComponent
	{path: '**', component: HomeComponent}

];

//procedimiento que necesita angular para poder cargar el Provider de la ruta y que todo funcione
export const appRoutingProviders: any[] = [];

//exportamos el routing / para que coja las rutas y las inyecte en el framework
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

