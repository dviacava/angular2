import { Injectable } from '@angular/core';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase/app';

import { Mensaje } from '../interfaces/mensaje';

@Injectable()
export class ChatService {
    chats: FirebaseListObservable<any[]>;
    usuario:any = {};

    constructor( public afAuth: AngularFireAuth, public db: AngularFireDatabase) {
        console.log(this.usuario);
        if( localStorage.getItem('usuario')){
            this.usuario = JSON.parse(localStorage.getItem('usuario'));
        }else {
            this.usuario = null;
        }
    }
    cargarMensajes() {
        this.chats = this.db.list('/chats', {
            query: {
                limitToLast: 20,
                orderByKey: true,
            }
        });
        return this.chats;
    }
    agregarMensaje( texto:string ){
        let mensaje:Mensaje = {
            nombre: this.usuario.displayName,
            mensaje: texto,
            uid: this.usuario.uid
        }

        return this.chats.push( mensaje );
    }
    login( proveedor: string ) {
        let _provider;
        if(proveedor == 'google') {
            _provider = new firebase.auth.GoogleAuthProvider()
        }
        else {
            if(proveedor == 'twitter'){
                
            }else{

            }
        }

        this.afAuth.auth.signInWithPopup(_provider).then( data => {
            console.log(data.user);
            this.usuario = data.user;
            localStorage.setItem('usuario', JSON.stringify(data));
        })
    } 

    logout() {
        localStorage.removeItem('usuario');
        this.usuario = null;
   
       this.afAuth.auth.signOut();
   
    }

}