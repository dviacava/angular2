import { NetflixPage } from './app.po';

describe('netflix App', () => {
  let page: NetflixPage;

  beforeEach(() => {
    page = new NetflixPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
