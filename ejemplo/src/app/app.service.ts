import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class AppService {
    contactos: FirebaseListObservable<any[]>;

    constructor (private af: AngularFireDatabase) {};

    getContactos() {
        this.contactos = this.af.list('/contactos') as FirebaseListObservable<any[]>;
        return this.contactos;
    }

    getContactosFiltro(filtro: string) {
        this.contactos = this.af.list('/contactos', {
            query: {
                orderByChild: 'direccion',
                equalTo: filtro
            }
        }) as FirebaseListObservable<any[]>;
        return this.contactos;
    }

    updateContacto(key, contacto){
        this.contactos.update(key, contacto);
    }

    removeContacto(key){
        this.contactos.remove(key);
    }

    addContacto(contacto){
        this.contactos.push(contacto);
    }
}