import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2'
//permisos e instalar promise-polyfill --save-exact
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppService } from './app.service';

import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';
import { DetallesComponent } from './detalles/detalles.component';
import { EditarComponent } from './editar/editar.component';
import { EliminarComponent } from './eliminar/eliminar.component';
import { AgregarComponent } from './agregar/agregar.component'

export const firebaseconfig = {
  apiKey: "AIzaSyCm4QgW26DK4qJq4fQ8gWKu4qdtKaKiHCs",
  authDomain: "angularfirebase-91a28.firebaseapp.com",
  databaseURL: "https://angularfirebase-91a28.firebaseio.com",
  projectId: "angularfirebase-91a28",
  storageBucket: "angularfirebase-91a28.appspot.com",
  messagingSenderId: "257089612560"
}

@NgModule({
  declarations: [
    AppComponent,
    DetallesComponent,
    EditarComponent,
    EliminarComponent,
    AgregarComponent
  ],
  entryComponents: [ //siempre y cuando cuando vayamos a utilizar el componente como cuadro de dialogo
    EliminarComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseconfig),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule // imports firebase/auth, only needed for auth features
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
